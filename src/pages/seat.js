import React from 'react';
import { ScrollView, Text, View, ImageBackground, style, FlatList } from 'react-native';
import { Tabs, Icon, Button, Grid } from '@ant-design/react-native';
import axios from 'axios'
import Item from '@ant-design/react-native/lib/list/ListItem';
import Seatmap from 'react-seatmap';

class Seat extends React.Component {
    state = {
        products: [],
        seats: []
    }

    UNSAFE_componentWillMount() {
        this.RenderAIp()
        this.RenderAIpSeat()

    }

    RenderAIp = () => {
        axios.get('https://zenon.onthewifi.com/ticGo/movies/cinemas')
            .then(response => {
                console.log(response)
                return response.data
            })
            .then(data => {
                this.setState({ products: data })
                console.log(data)
            })
            .catch(e => console.log(e.response))
    }
    RenderAIpSeat = () => {
        axios.get('https://zenon.onthewifi.com/ticGo/movies/showtimes')
            .then(response => {
                console.log(response)
                return response.data
            })
            .then(data => {
                this.setState({ seats: data })
                console.log(data)
            })
            .catch(e => console.log(e.response))
    }

    testRender = (seats) => {
        for (let i of seats) {
            for (let j = 0; j < i.rows; j++) {
                for (let k = 0; k < i.columns; k++) {
                    return <Button>Test</Button>
                }
            }
        }
    }

    renderContent = (tab, index, source) => {
        const style = {
            paddingVertical: 5,
            justifyContent: 'center',
            alignItems: 'center',
            //margin: 10,

        };
        const pic = {
            height: 150, width: 150, justifyContent: 'center', margin: 5, borderRadius: 10, borderColor: 'gold', borderWidth: 1,
        };
        const middleCon = {
            flexDirection: 'row'
        };
        const textStylejisu = {
            backgroundColor: 'black',
            borderRadius: 50,
            color: 'gold',
            paddingHorizontal: 10,
            margin: 10,
            borderColor: 'gold',
            borderWidth: 0.5,
            textAlign: 'center'
        };
        const textStyleInfojisu = {
            backgroundColor: 'white',
            borderRadius: 50,
            color: 'black',
            paddingHorizontal: 10,
            margin: 10,
            borderColor: 'gold',
            borderWidth: 0.5,
            textAlign: 'center'
        }

        const content = this.state.products.map(product => {
            const seats = this.state.seats
            return (
                <View key={`${index}_${product}`} style={style}>
                    <View style={{ backgroundColor: 'black', width: '100%' }}>
                        <Text style={[textStylejisu, { fontSize: 20 }]}>
                            {product.name}
                        </Text>

                        <View style={{ flexDirection: 'row' }}>
                            {product.seats.map(item => {
                                return (
                                    <View>

                                        <View>
                                            <Seatmap rows={3} maxReservableSeats={3} alpha />,
                                     </View>
                                        <Text style={[textStyleInfojisu]}>
                                            <Text style={{ fontSize: 12 }}>
                                                {item.price} {item.name}
                                            </Text>
                                        </Text>
                                    </View>
                                )
                            })}
                        </View>
                    </View>
                </View>
            );
        });
        return <ScrollView style={{}}>{content}</ScrollView>;
    }
    render() {
        const tabs2 = [
            {
                title: 'Show Time'
            }
        ];
        const style = {
            height: 150,

        };
        return (
            <View
                style={{ flex: 1, backgroundColor: 'black', color: 'white' }}>
                <ImageBackground source={require('../hallway.jpg')}
                    style={{ width: '100%', height: '100%', flex: 1 }} >
                    <View style={{ flex: 2 }}>
                        <Tabs
                            style={style}
                            tabs={tabs2}
                            initialPage={1}
                            tabBarPosition="top"
                            tabBarTextStyle={{ color: 'gold' }}
                            tabBarBackgroundColor={{ color: 'black' }}>
                            {this.renderContent}
                        </Tabs>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}

export default Seat