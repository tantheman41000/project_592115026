import React from 'react';
import { ScrollView, Text, View, Image, style, ImageBackground, TouchableOpacity } from 'react-native';
import { Tabs, Icon, Button } from '@ant-design/react-native'
import { IconFill, IconOutline } from "@ant-design/icons-react-native";
import axios from 'axios'




export default class Home extends React.Component {

    state = {
        products: []
    }

    UNSAFE_componentWillMount() {
        axios.get('https://zenon.onthewifi.com/ticGo/movies')
            .then(response =>  response.data )
            .then(data => {
                this.setState({ products: data })
            })
    }
    renderContent = (tab, index, source) => {
        const style = {
            paddingVertical: 5,
            justifyContent: 'center',
            alignItems: 'center',
            //margin: 10,

        };
        const pic = {
            height: 150, width: 150, justifyContent: 'center', margin: 5, borderRadius: 10, borderColor: 'gold', borderWidth: 1,
        };
        const middleCon = {
            flexDirection: 'row'
        };
        const textStylejisu = {
            backgroundColor: 'black',
            borderRadius: 50,
            color: 'gold',
            paddingHorizontal: 10,
            margin: 10,
            borderColor: 'gold',
            borderWidth: 0.5,
            textAlign: 'center'
        }


        const content = this.state.products.map(product => {
            return (
                <View key={`${index}_${product}`} style={style}>
                    <View style={{ backgroundColor: 'black', width: '100%' }}>
                        <Text style={[textStylejisu, { fontSize: 20 }]}>
                            {product.name}
                        </Text>
                    </View>
                    <View style={middleCon}>
                        <Image style={pic} source={{ uri: product.image }} />
                        <View >
                            <Icon name="clock-circle" size={'md'} />
                            <Text style={textStylejisu}>{product.duration} mins</Text>
                            <TouchableOpacity>
                                <Button type="ghost" className="am-button-borderfix" style={{ color: 'red', backgroundColor: '#ffccff' }}>GET TICKET</Button>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            );
        });
        return <ScrollView style={{}}>{content}</ScrollView>;
    };

    render() {
        const tabs2 = [
            {
                title: 'Show Time'
            }
        ];
        const style = {
            height: 150,

        };
        return (
            <View style={{ flex: 1, backgroundColor: 'black', color: 'white' }}>
                <ImageBackground source={require('../hallway.jpg')}
                    style={{ width: '100%', height: '100%', flex: 1 }} >
                    <View style={{ flex: 2 }}>
                        <Tabs style={style} tabs={tabs2} initialPage={1} tabBarPosition="top" tabBarTextStyle={{ color: 'gold' }} tabBarBackgroundColor={{ color: 'black' }}>
                            {this.renderContent}
                        </Tabs>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}
export const title = 'Tabs';
export const description = 'Tabs example';