import React, { Component } from 'react';
import { Image, FlatList, Text, View, Alert, ImageBackground, TouchableOpacity, ScrollView } from 'react-native';
import { connect } from 'react-redux'
import styles from '../utilities/styles'
import NavigationBar from 'react-native-navbar';
import { Button, Icon } from '@ant-design/react-native';
import { push } from 'connected-react-router'
import { CheckBox } from 'react-native-elements'
import SOFA from '../images/seat3.png'
import SOFA_CHECKED from '../images/seat3_check.png'
import PREMIUM from '../images/seat2.png'
import PREMIUM_CHECK from '../images/seat2_check.png'

const rows = (
  {
    number: '10',
    isReserved: false
  }
)

class seatSelect extends Component {
  state = {
    movie: [],
    seats: [],
    sofa: [],
    premium: [],
    deluxe: [],
    booking: [],
    Checked: true,
    Checked_P:true,
    Checked_D:true
  }
  UNSAFE_componentWillMount() {
    console.log('Prop movie:', this.props.location);
    this.setState({ movie: this.props.location.state.movie, seats: this.props.location.state.movie.seats })
    this.setState({ sofa: this.props.location.state.movie.seats[0], premium: this.props.location.state.movie.seats[1], deluxe: this.props.location.state.movie.seats[2] })
  }

  renderImage = () => {
    var imgSource = this.state.Checked? SOFA : SOFA_CHECKED;
    return (
      <View>
        <Image
          style={styles.seat3B}
          source={imgSource}
        />
      </View>
    );
  }

  renderImagerow2 = () => {
    var imgSourceB = this.state.Checked_P? PREMIUM : PREMIUM_CHECK;
    return (
      <View>
        <Image
          style={styles.seat2B}
          source={imgSourceB}
        />
      </View>
    );
  }

  renderImagerow3 = () => {
    var imgSourceC = this.state.Checked_D? SOFA : SOFA_CHECKED;
    return (
      <View>
        <Image
          style={styles.seat1B}
          source={imgSourceC}
        />
      </View>
    );
  }

  addZero(time) {
    if (time < 10) {
      time = "0" + time;
    }
    return time;
  }
  goToSummary = () => {
    this.props.history.push('./summary', { movie: this.state.movie, seats: this.state.booking })
  }
  bookingChack = (row, col, type) => {
    console.log(row, col, type);
    this.setState({
      booking: [...this.state.booking, {
        type: type,
        row: row,
        column: col
      }]
    })

  }
  render() {
    // console.log('State Movie:', this.state.movie)
    // console.log('State Seate:', this.state.seats)
    // console.log('Sofa:', this.state.sofa)
    // console.log('pre:', this.state.premium)
    // console.log('delux:', this.state.deluxe)
    return (
      <View style={styles.container}>
        <View style={styles.header} >
          <View style={{ flex: 1, }}>
            <NavigationBar title={{ title: 'SeatSelect' }}
              leftButton={{
                title: 'Back',
                handler: () => this.props.history.push('./menu', { index: 0 })
              }} />
          </View>
        </View>
        <View style={{
          flex: 11, flexDirection: 'column',
          backgroundColor: '#b9b9b9',
          justifyContent: 'center',
        }}>
          <ImageBackground
            blurRadius={5}
            source={{ uri: this.state.movie.movie.image }}
            style={{
              flex: 1,
              flexDirection: 'row',
              height: 200,
              width: '100%',
              justifyContent: 'center',
            }}
          >
            <View style={{
              flex: 1.5,
              alignItems: 'center',
              marginTop: 10,
            }}>
              <Image
                source={{ uri: this.state.movie.movie.image }}
                style={{ width: '100%', height: 180, resizeMode: 'contain' }}
              />
            </View>
            <View style={{ flex: 2, marginTop: 70, alignItems: 'center' }}>
              <Text style={{
                textAlign: 'center',
                fontSize: 18,
                color: 'white',
              }}>
                {this.state.movie.movie.name}
              </Text>
              <Text style={{
                textAlign: 'center',
                fontSize: 15,
                color: 'white',
              }}>
                <Icon name="clock-circle" size="xxs" color="white" /> {this.state.movie.movie.duration} min
              </Text>
              <Text style={{
                textAlign: 'center',
                fontSize: 15,
                color: 'white',
              }}>
                {this.addZero(new Date(this.state.movie.startDateTime).getHours())}
                :{this.addZero(new Date(this.state.movie.startDateTime).getMinutes())} - {this.addZero(new Date(this.state.movie.endDateTime).getHours())}
                :{this.addZero(new Date(this.state.movie.endDateTime).getMinutes())}
              </Text>
            </View>
          </ImageBackground>
          <View style={{
            flex: 1.85,
            flexDirection: 'row',
          }}>
            <ScrollView>
              <View style={{
                flex: 1,
                margin: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
                <Image style={styles.screen} source={require('../images/screen2.png')} />
                {
                  this.state.seats.map((i, index) => {
                    return Object.keys(i).map((rowsData, index) => {
                      // console.log('i: ', rowsData)
                      // console.log('i: ', index)
                      return (
                        <FlatList
                          data={i['row' + (index + 1)]}
                          numColumns={i.columns}
                          extraData={this.state}
                          renderItem={(item, index) => {
                            const rowTes = [index + 1]
                            // { console.log('Row:Columns', item.index + 1,); }
                            if (i.type === "SOFA") {
                              return (
                                <TouchableOpacity disabled={item.item}
                                  onPress={() => {
                                    console.log('row:', rowTes[0],
                                    ' col:', item.index + 1,
                                    'type:', i.type),
                                    console.log(this.state.Checked)
                                    this.bookingChack(
                                      rowTes[0],
                                      item.index + 1,
                                      i.type
                                    );
                                    this.setState({
                                      Checked: !this.state.Checked
                                    });
                                  }}>
                                  {this.renderImage()}
                                </TouchableOpacity>
                              )
                            } else if (i.type === "PREMIUM") {
                              return (
                                <TouchableOpacity disabled={item.item} onPress={() => { 
                                  console.log('row:', rowTes[0], ' col:', 
                                  item.index + 1, 'type:', i.type),
                                   this.bookingChack(rowTes[0],
                                    item.index + 1, i.type),
                                    this.setState({
                                      Checked_P: !this.state.Checked_P
                                    });
                                    }}>
                                  {this.renderImagerow2()}
                                </TouchableOpacity>
                              )
                            } else {
                              return (
                                <TouchableOpacity disabled={item.item} 
                                onPress={() => {
                                console.log('row:', rowTes[0], ' col:', item.index + 1, 'type:', i.type),
                                 this.bookingChack(rowTes[0], 
                                 item.index + 1, i.type) 
                                 this.setState({
                                  Checked_D: !this.state.Checked_D
                                });
                                 }}>
                                  {this.renderImagerow3()}
                                </TouchableOpacity>
                              )
                            }
                          }} />
                      )
                    })
                  }).reverse()
                }
              </View>
            </ScrollView>
            {/* <Text style={{ fontSize: 60 }}>ที่นั่งทำยังไงครับ</Text> */}
          </View>
        </View>
        <View>
          <Button type='primary' onPress={() => { this.goToSummary() }}>Booking</Button>
        </View>
      </View>
    )
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    push: location => {
      dispatch(push(location))
    }
  }
}
export default connect(null, { push })(seatSelect)