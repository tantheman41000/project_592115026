import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-native'
import { Provider } from 'react-redux'
import { store, history } from './AppStore'
import { ConnectedRouter } from 'connected-react-router'

import LoginPage from '../page/login'
import Menu from '../page/menu'
import ListMovie from '../page/listMovie'
import HistoryTicket from '../page/historyTicket'
import Profile from '../page/profile'
import EditProfile from '../page/editProfile'
import SeatSelect from '../page/seatSelect'
import Register from '../page/register'
import Summary from '../page/summary'
export default class Router extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Switch>
            <Route exact path="/login" component={LoginPage} />
            <Route exact path="/menu" component={Menu} />
            <Route exact path="/listMovie" component={ListMovie} />
            <Route exact path="/historyTicket" component={HistoryTicket} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/editProfile" component={EditProfile} />
            <Route exact path="/seatSelect" component={SeatSelect} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/summary" component={Summary} />
            <Redirect to='/login' />
          </Switch>
        </ConnectedRouter>
      </Provider>

    )
  }
}